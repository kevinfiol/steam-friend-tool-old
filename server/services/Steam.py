import requests

class Steam():
    """ Steam Web API class """
    api_key = None
    api_query = 'http://api.steampowered.com/{iface}/{command}/{version}/'
    store_query = 'http://store.steampowered.com/api/{command}/'

    def __init__(self, api_key=None):
        self.api_key = api_key

    def api_call(self, iface, command, version, **kwargs):
        query = self.api_query.format(iface=iface, command=command, version=version)
        params = {'key': self.api_key}
        params.update(kwargs)
        res = requests.get(query, params)
        return res.text

    def store_call(self, command, **kwargs):
        query = self.store_query.format(command=command)
        res = requests.get(query, kwargs)
        return res.text