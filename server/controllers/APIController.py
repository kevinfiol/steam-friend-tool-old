import json
from server.models.App import App
from server.models.Category import Category
from sqlalchemy.exc import IntegrityError, DataError

class APIController():
    steam = None
    db = None

    def __init__(self, steam, db):
        self.steam = steam
        self.db = db

    def load_args(self, request):
        args = request.data

        if not isinstance(request.data, str):
            args = request.data.decode('utf-8')
        return json.loads(args)

    def create_app(self, data):
        return App(
            data['steam_appid'],
            data['name'],
            data['categories'],
            data['header_image'],
            data['is_free'],
            data['platforms']
        )

    def save_row(self, row):
        try:
            self.db.session.add(row)
            self.db.session.commit()
        except IntegrityError:
            pass

    def api_call(self, request, iface, command, version):
        kwargs = self.load_args(request)
        response = self.steam.api_call(iface, command, version, **kwargs)
        return response

    def store_call(self, request, command):
        kwargs = self.load_args(request)
        response = self.steam.store_call(command, **kwargs)
        return response

    def get_all_categories(self):
        category_list = [cat.serialize() for cat in Category.query.all()]
        category_dict = {}

        for cat in category_list:
            category_dict[cat['category_id']] = cat['description']
        return category_dict

    def store_appdetails(self, request):
        kwargs = self.load_args(request)

        if 'appids' in kwargs:
            app = App.query.get(kwargs['appids'])

            if app is not None:
                return app.format_dict()
            else:
                raw_data = self.steam.store_call('appdetails', **kwargs)
                app_data = json.loads(raw_data)[kwargs['appids']]

                if 'data' in app_data and 'categories' in app_data['data']:
                    new_app = self.create_app(app_data['data'])

                    for cat in app_data['data']['categories']:
                        if Category.query.get(cat['id']) is None:
                            new_category = Category(cat['id'], cat['description'])
                            self.save_row(new_category)

                    self.save_row(new_app)
                    return new_app.format_dict()
        return None