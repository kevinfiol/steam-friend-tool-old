import json
from flask import render_template, request, jsonify, Blueprint
from server import steam, db
from server.controllers.APIController import APIController

bp = Blueprint('routes', __name__, template_folder='templates')

api_controller = APIController(steam, db)

@bp.route('/', defaults={'path': ''})
@bp.route('/<path:path>')
def index(path):
    return render_template('index.html')

@bp.route('/steamAPI/<iface>/<command>/<version>/', methods=['POST'])
def api_call(iface, command, version):
    response = api_controller.api_call(request, iface, command, version)
    return jsonify(response)

@bp.route('/storeAPI/interface/<command>/', methods=['POST'])
def store_call(command):
    response = api_controller.store_call(request, command)
    response_json = json.dumps(response)
    return jsonify(response_json)

@bp.route('/storeAPI/app/appdetails/', methods=['POST'])
def store_appdetails():
    response = api_controller.store_appdetails(request)
    response_json = json.dumps(response)
    return jsonify(response_json)

@bp.route('/client/getAllCategories/', methods=['POST'])
def get_all_categories():
    response = api_controller.get_all_categories()
    response_json = json.dumps(response)
    return jsonify(response_json)