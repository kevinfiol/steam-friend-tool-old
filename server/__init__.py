from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from server.config import Config
from server.secret import KEYS
from server.services.Steam import Steam

db = SQLAlchemy()
migrate = Migrate()
steam = Steam(KEYS['STEAM_API'])

def create_app(config=Config):
    app = Flask(__name__)
    app.config.from_object(config)

    db.init_app(app)
    migrate.init_app(app, db)

    from server import routes
    app.register_blueprint(routes.bp)
    return app