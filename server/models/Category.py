from server.models import BaseModel
from server import db

class Category(BaseModel):
    __tablename__ = 'category'
    category_id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String())

    def __init__(self, category_id, description):
        self.category_id = category_id
        self.description = description