from server.models import BaseModel
from server import db
from sqlalchemy.dialects.postgresql import JSON, ARRAY

class App(BaseModel):
    __tablename__ = 'app'
    steam_appid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    categories = db.Column(ARRAY(db.Integer))
    header_image = db.Column(db.String())
    is_free = db.Column(db.Boolean)
    platforms = db.Column(JSON)

    def __init__(self, steam_appid, name, categories, header_image, is_free, platforms):
        self.steam_appid = steam_appid
        self.name = name
        self.categories = self.parse_categories(categories)
        self.header_image = header_image
        self.is_free = is_free
        self.platforms = platforms
    
    def parse_categories(self, categories):
        # Categories are stored as an array of Category IDs
        return [category['id'] for category in categories]

    def format_dict(self):
        return {
            'steam_appid': self.steam_appid,
            'name': self.name,
            'categories': self.categories,
            'header_image': self.header_image,
            'is_free': self.is_free,
            'platforms': self.platforms
        }