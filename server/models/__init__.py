import json
from server import db

class BaseModel(db.Model):
    __abstract__ = True

    def serialize(self):
        return {
            column.key: getattr(self, attr) 
                for attr, column in self.__mapper__.c.items()
        }

# Import Models of server.models
from server.models.App import App
from server.models.Category import Category