from server import create_app, db
db.session.query
app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {'app': app, 'db': db}