import m from 'mithril';
import { gstate } from 'gstate';

const GameEl = {
    view({attrs}) {
        return m('.col.col-6.md-col-3.p1.my1', [
            m('a', {
                href: `http://store.steampowered.com/app/${attrs.game.steam_appid}`
            }, [
                m('img.rounded.fit', {
                    src: attrs.game.header_image,
                    onerror: ev => ev.target.src = '/static/img/steam.png' 
                })
            ]),

            m('.flex.flex-column.truncate', [
                m('span.h4.truncate', attrs.game.name),
                m('span.h4.truncate', attrs.game.steam_appid),
            ]),

            // Platform Icons
            Object.keys(attrs.game.platforms).map((key) => {
                return attrs.game.platforms[key]
                    ? m('img', { src: `/static/img/${key}.png` })
                    : null
                ;
            })
        ]);
    }
};

export const GameList = {
    view() {
        return [
            m('.h3', `you have ${gstate.filteredGames().length} games in common`),

            gstate.filteredGames() && gstate.filteredGames().length
                ? gstate.filteredGames().map(game => m(GameEl, { game: game }))
                : m('.h2.p4.my3.center.border.rounded', 'no games found')
            ,
        ];
    }
}