import m from 'mithril';
import stream from 'mithril/stream';
import { gstate } from 'gstate';
import { Checkbox } from 'components/Checkbox';
import { RadioButton } from 'components/RadioButton';

export const PlatformFilter = {
    platforms: null,

    oninit({state}) {
        state.platforms = ['windows', 'mac', 'linux'];
    },

    view({state}) {
        return [
            m('.h3', 'platforms'),

            m('.col.col-12.p1.my1.flex', [
                m('.flex.flex-column.flex-auto', [
                    m(Checkbox, {
                        checked: gstate.enablePlatformFilter(),
                        onchange: () => gstate.enablePlatformFilter(!gstate.enablePlatformFilter()),
                        label: 'enable platform filter?'
                    }),

                    m('.flex-column', [
                        state.platforms.map((platform) => {
                            return m(RadioButton, {
                                name: 'platform',
                                checked: gstate.platformFilter() === platform,
                                onchange: () => gstate.platformFilter(platform),
                                label: platform,
                                disabled: !gstate.enablePlatformFilter()
                            })
                        })
                    ])
                ])
            ])
        ];
    }
};