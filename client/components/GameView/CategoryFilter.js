import m from 'mithril';
import stream from 'mithril/stream';
import { gstate } from 'gstate';
import { Checkbox } from 'components/Checkbox';



export const CategoryFilter = {
    checkToggle: false,
    categories: null,
    categoryObjs: null,
    multiplayer: [1, 9, 20, 27, 36, 38],

    oninit({state}) {
        const reduced = gstate.games().reduce((acc, current) => {
            return [...acc, ...current.categories];
        }, []);

        // Temporary Set to de-dup category values
        const categorySet = new Set(reduced);

        state.categories = Array.from(categorySet);
        gstate.categoryFilter([...state.categories]);

        state.categoryObjs = state.categories.map((cat) => {
            return { label: gstate.categoryDict[cat], id: cat };
        });

        state.categoryObjs.sort((a, b) => a.label.toUpperCase() > b.label.toUpperCase());
    },

    view({state}) {
        return [
            m('h3', 'categories'),

            m('.button.btn.btn-outline.m1', {
                onclick: () => {
                    if (state.checkToggle) gstate.categoryFilter([...state.categories]);
                    else gstate.categoryFilter([]);
                    
                    state.checkToggle = !state.checkToggle;
                }
            }, state.checkToggle ? 'check all' : 'uncheck all'),

            m('.button.btn.btn-outline.m1', {
                onclick: () => {
                    const multiplayer = state.multiplayer.filter(g => state.categories.indexOf(g) > -1);
                    gstate.categoryFilter([...multiplayer]);
                }
            }, 'check online multi-player games'),

            m('.clearfix.p2.my2', [
                state.categoryObjs.map((category) => {
                    return m('.col.col-12.sm-col-4', [
                        m(Checkbox, {
                            data: category.id,
                            checked: gstate.categoryFilter().indexOf(category.id) > -1,
                            onchange: () => {
                                const temp = gstate.categoryFilter();
                                
                                if (temp.indexOf(category.id) > -1) {
                                    temp.splice(temp.indexOf(category.id), 1);
                                    gstate.categoryFilter(temp);
                                } else {
                                    temp.push(category.id);
                                    gstate.categoryFilter(temp);
                                }
                            },
                            label: category.label
                        })      
                    ]);
                })
            ])
        ];
    }
};