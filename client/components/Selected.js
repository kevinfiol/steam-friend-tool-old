import m from 'mithril';
import { gstate } from 'gstate';

const FriendEl = {
    view({attrs}) {
        return m('.col.col-12.md-col-6.p1.my1.flex', [
            m('.flex', [
                m('img.rounded', { src: attrs.friend.avatarmedium })
            ]),
            
            m('.flex.flex-column.mx1.truncate.flex-auto', [
                m('span.h5', attrs.friend.personaname),
                m('span.h5', attrs.friend.steamid),
                m('a.h5.teal.truncate', { 
                    href: attrs.friend.profileurl 
                }, attrs.friend.profileurl)
            ]),

            !gstate.games()
                ? m('button.btn.btn-small.btn-outline', {
                    onclick: () => gstate.selected.splice(attrs.index, 1)
                }, m('span.h5', 'remove'))
                : null
            ,
        ])
    }
};

export const Selected = {
    view() {
        return [
            m('h3', 'your friends'),
            m('.clearfix', [
                gstate.selected.length > 0
                    ? gstate.friends.map((friend) => {
                        let index = gstate.selected.indexOf(friend.steamid);
                        if (index > -1) {
                            return m(FriendEl, { friend: friend, index: index });
                        }
                    })
                    : m('.h3.col.col-12.border.rounded.p3.my1.center', 'no friends currently selected')
                ,
            ])
        ];
    }
};