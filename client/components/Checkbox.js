import m from 'mithril';

export const Checkbox = {
    view({attrs}) {
        return m('label.form-checkbox', [
            m('input[type="checkbox"]', {
                data: attrs.data || null,
                checked: attrs.checked,
                onchange: attrs.onchange
            }),
            m('i.form-icon'),
            m('span.truncate', attrs.label || '')
        ]);
    }
};