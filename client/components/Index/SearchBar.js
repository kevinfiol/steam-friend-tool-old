import m from 'mithril';
import stream from 'mithril/stream';
import { gstate } from 'gstate';
import { Steam } from 'tools/Steam';
import { reducePromises } from 'tools/util';

export const SearchBar = {
    input: null,

    oninit({state}) {
        state.input = stream('');
    },

    view({state}) {
        return [
            m('label', [
                'enter your steam64 id, or your user id: ',
                m('span.italic', ['http://steamcommunity.com/id/', m('span.bold', '[user_id]')])
            ]),
            m('input.input.bg-black.white.my1', {
                type: 'text',
                oninput: m.withAttr('value', state.input),
                onkeyup: (ev) => {
                    if (ev.keyCode === 13 && state.input()) {
                        resolveAndGetFriends(state.input());
                    }
                }
            }),

            m('.clearfix', [
                m('button.btn.btn-outline.left', {
                    disabled: !state.input(),
                    onclick: () => { 
                        if (state.input()) resolveAndGetFriends(state.input());
                    }
                }, 'search'),

                gstate.selected.length
                    ? m('button.btn.btn-outline.right', {
                        onclick: () => routeToGameView()
                    }, 'compare games')
                    : null
                ,
            ])
        ];
    }
};

function routeToGameView() {
    m.route.set(`${gstate.steamid}/:key`, {
        key: gstate.selected.toString()
    });
}

function resolveAndGetFriends(input) {
    gstate.clearData();
    gstate.loading(true);

    const promises = [
        () => {
            gstate.progressMsg('resolving id...');
            return resolveID(input.trim())
                .then(res => gstate.steamid = res)
            ;
        },
        (id) => {
            gstate.progressMsg('fetching user data...');
            return getUser(id).then((res) => {
                gstate.user = res[0].players[0];
                return res[1].friends;
            });
        },
        (friends) => {
            gstate.progressMsg('fetching friends list...');
            return getFriendSummaries(friends)
                .then(res => gstate.friends = res)
            ;
        }
    ];

    reducePromises(promises)
        .then(() => {
            gstate.loading(false);
            gstate.progress(null);
            gstate.progressMsg(null);
            m.redraw();
        })
        .catch((err) => {
            console.log(err);
            gstate.error(1);
            gstate.loading(false);
            gstate.progress(null);
            gstate.progressMsg(null);
            m.redraw();
        })
    ;
}

function resolveID(id) {
    return isNaN(id)
        ? Steam.resolveVanityURL(id)
        : Promise.resolve(id)
    ;
}

function getUser(id) {
    return Promise.all([
        Steam.getPlayerSummaries(id),
        Steam.getFriendList(id)
    ]);
}

function getFriendSummaries(friends) {
    let idString = friends.map(x => x.steamid).toString();

    return Steam.getPlayerSummaries(idString)
        .then((res) => {
            return res.players.sort((a, b) => {
                return a.personaname.toUpperCase() > b.personaname.toUpperCase();
            });
        })
    ;
}