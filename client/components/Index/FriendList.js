import m from 'mithril';
import { gstate } from 'gstate';

const FriendEl = {
    view({attrs}) {
        return m('.col.col-12.sm-col-6.md-col-3.p1.my1.flex', [
            m('.flex', [
                m('img.rounded', { src: attrs.friend.avatar })
            ]),
            
            m('.flex.flex-column.mx1.truncate.flex-auto', [
                m('span.h5.truncate', attrs.friend.personaname),
                m('span.h6.truncate', attrs.friend.steamid)
            ]),

            m('button.btn.btn-small.btn-outline', {
                disabled: gstate.selected.indexOf(attrs.friend.steamid) > -1,
                onclick: () => {
                    if (gstate.selected.indexOf(attrs.friend.steamid) < 0
                        && gstate.selected.length < 4
                    ) {
                        gstate.selected.push(attrs.friend.steamid);
                    }
                }
            }, m('span.h6', 'add'))
        ]);
    }
};

export const FriendList = {
    view() {
        return [
            m('h3', 'friends list'),
            gstate.friends.map(friend => m(FriendEl, { friend: friend }))
        ];
    }
};