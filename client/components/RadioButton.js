import m from 'mithril';

export const RadioButton = {
    view({attrs}) {
        return m('label.form-radio', [
            m('input[type="radio"]', {
                data: attrs.data || null,
                name: attrs.name,
                checked: attrs.checked,
                onchange: attrs.onchange,
                disabled: attrs.disabled
            }),
            m('i.form-icon'),
            m('span.truncate', attrs.label || '')
        ]);
    }
};