import m from 'mithril';
import { gstate } from 'gstate';

export const User = {
    view() {
        return [
            m('h3', 'you'),
            m('.col.col-12.p1.my1.flex', [
                m('.flex', [
                    m('img.rounded', { src: gstate.user.avatarmedium })
                ]),

                m('.flex.flex-column.mx1.truncate.flex-auto', [
                    m('span.h5', gstate.user.personaname),
                    m('span.h5', gstate.user.steamid),
                    m('a.h5.teal.truncate', {
                        href: gstate.user.profileurl
                    }, gstate.user.profileurl)
                ])
            ])
        ];
    }
};