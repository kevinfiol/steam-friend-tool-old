import m from 'mithril';
import { gstate } from 'gstate';
import { Layout } from 'views/Layout';
import { Index } from 'views/Index';
import { GameView } from 'views/GameView';
import './styles/main.css';

gstate.init();
m.route.prefix('');

m.route(document.getElementById('app'), '/', {
    '/': {
        render() {
            return m(Layout, m(Index));
        }
    },
    '/:steamid/:key': {
        render({attrs, key}) {
            return m(Layout, m(GameView, { steamid: attrs.steamid, key: key }));
        }
    }
});