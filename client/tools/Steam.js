import m from 'mithril';
import { parseJSON } from 'tools/util';

export const Steam = {
    resolveVanityURL(vanityurl) {
        return m.request({
            method: 'POST',
            url: '/steamAPI/ISteamUser/ResolveVanityURL/v0001/',
            data: { vanityurl: vanityurl }
        }).then((res) => {
            res = JSON.parse(res);

            if (res.response.success === 1) {
                return res.response.steamid;
            }

            throw('Invalid Vanity ID');
        });
    },

    getPlayerSummaries(steamids) {
        return m.request({
            method: 'POST',
            url: '/steamAPI/ISteamUser/GetPlayerSummaries/v0002/',
            data: { steamids: steamids }
        }).then((res) => {
            res = parseJSON(res);

            if (res && Object.keys(res).length > 0) {
                return { players: res.response.players };
            }

            throw('Invalid IDs');
        });
    },

    getFriendList(steamid) {
        return m.request({
            method: 'POST',
            url: '/steamAPI/ISteamUser/GetFriendList/v0001/',
            data: { steamid: steamid, relationship: 'friend' }
        }).then((res) => {
            res = parseJSON(res);

            if (res && Object.keys(res).length > 0) {
                return { friends: res.friendslist.friends };
            }

            throw('Invalid ID');
        });
    },

    getOwnedGames(steamid) {
        return m.request({
            method: 'POST',
            url: '/steamAPI/IPlayerService/GetOwnedGames/v0001/',
            data: {
                steamid: steamid,
                include_appinfo: 1,
                include_played_free_games: 1
            }
        }).then(res => parseJSON(res));
    },

    getAppDetails(appids) {
        return m.request({
            method: 'POST',
            url: '/storeAPI/app/appdetails/',
            data: {
                appids: appids.toString(),
                filters: 'basic,categories,platforms'
            }
        }).then(res => parseJSON(res));
    },

    getAllCategories() {
        return m.request({
            method: 'POST',
            url: '/client/getAllCategories/'
        }).then(res => parseJSON(res));
    }
};