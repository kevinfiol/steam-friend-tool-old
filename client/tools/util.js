const reducePromises = (promises, load) => {
    const initial = Promise.resolve();
    const total = promises.length;
    const progress = { current: 0, percent: 0 };

    if (load) load(progress);

    return promises.reduce((sum, current) => {
        return sum = sum.then(current).then((res) => {
            progress.current += 1;
            progress.percent = progress.current / total;
            return res;
        });
    }, initial);
};

const parallelPromises = (promises, load) => {
    const total = promises.length;
    const progress = { current: 0, percent: 0 };
    if (load) load(progress);

    promises.forEach((promise) => {
        promise.then(() => {
            progress.current += 1;
            progress.percent = progress.current / total;
        });
    });

    return Promise.all(promises);
};

const parseJSON = (json) => {
    try {
        return JSON.parse(json);
    } catch (e) {
        return null;
    }
};

export {
    parallelPromises,
    reducePromises,
    parseJSON
};