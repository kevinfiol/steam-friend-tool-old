import m from 'mithril';
import { gstate } from 'gstate';
import { Steam } from 'tools/Steam';
import { User } from 'components/User';
import { Selected } from 'components/Selected';
import { GameList } from 'components/GameView/GameList';
import { PlatformFilter } from 'components/GameView/PlatformFilter';
import { CategoryFilter } from 'components/GameView/CategoryFilter';
import { reducePromises, parallelPromises } from 'tools/util';

export const GameView = {
    oninit({attrs}) {

        
        if (!gstate.games()) {
            gstate.loading(true);
            gstate.progressMsg('looking up data...');
            getGameViewData(attrs.steamid, attrs.key);
            setTimeout(() => m.redraw());
        }
    },

    view() {
        return [
            gstate.user
                ? m(User)
                : null
            ,

            gstate.friends
                ? m(Selected)
                : null
            ,

            gstate.games() && gstate.games().length && gstate.categoryDict
                ? [m(PlatformFilter), m(CategoryFilter)]
                : null
            ,

            gstate.games() && gstate.games().length
                ? m(GameList)
                : m('.h2.p4.my3.center.border.rounded', 'no games found')
            ,
        ];
    }
};

function getGameViewData(steamid, friendsCSV) {
    const promises = [];
    const idString = `${steamid},${friendsCSV}`;

    if (gstate.steamid) {
        promises.push(() => getGames([gstate.steamid, ...gstate.selected]));
    } else {
        promises.push(() => {
            gstate.progressMsg('fetching user data...');

            return getSummaries(steamid, idString)
                .then((res) => {
                    gstate.steamid = res.steamid;
                    gstate.user = res.user;
                    gstate.friends = res.friends;
                    gstate.selected = res.selected;

                    return getGames([res.steamid, ...res.selected]);
                })
            ;
        });
    }

    promises.push(...[
        (intersection) => {
            gstate.progressMsg('fetching games...');
            return getGameDetails(intersection, gstate.progress)
                .then(res => gstate.games(res))
            ;
        },
        () => {
            gstate.progressMsg('collecting categories...');
            return Steam.getAllCategories()
                .then(res => gstate.categoryDict = res)
            ;
        }
    ]);

    reducePromises(promises)
        .then(() => {
            gstate.loading(false);
            gstate.progress(null);
            gstate.progressMsg(null);
            m.redraw();
        })
        .catch((err) => {
            console.log(err);
            gstate.error(2);
            gstate.loading(false);
            gstate.progress(null);
            gstate.progressMsg(null);
            m.redraw();
        })
    ;
}

function getSummaries(steamid, idString) {
    return Steam.getPlayerSummaries(idString)
        .then((res) => {
            // Get User Information
            // findIndex(p => p.steamid === steamid);
            const index = res.players.indexOf(res.players.filter(p => p.steamid === steamid)[0]);
            const user = res.players[index];

            // Remove User's Summary & Assign Friends
            res.players.splice(index, 1);
            const friends = res.players;
            const selected = friends.map(friend => friend.steamid);

            return {
                steamid: steamid,
                user: user,
                friends: friends,
                selected: selected
            };
        })
    ;
}

function getGames(ids) {
    return Promise.all(ids.map(id => Steam.getOwnedGames(id)))
        .then((res) => {
            // Check for empty libraries
            for (let i of res) {
                if (!i.response.games) return [];
            }

            const gameArrs = res.map((obj) => {
                return obj.response.games.map(game => game.appid);
            });

            return getIntersection(gameArrs);
        })
    ;
}

function getGameDetails(intersection, progressStream) {
    return parallelPromises(
        intersection.map(appid => Steam.getAppDetails(appid)),
        progress => progressStream(progress)
    )
        .then((res) => {
            const games = res.filter(g => g !== null);

            // Return Sorted Games
            return games.sort((a, b) => {
                return a.name.toUpperCase() > b.name.toUpperCase()
            });
        })
    ;
}

function getIntersection(arrs) {
    return arrs.reduce((a, b) => {
        return a.filter(appid => b.indexOf(appid) > -1);
    });
}