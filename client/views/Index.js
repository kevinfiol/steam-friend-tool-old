import m from 'mithril';
import { gstate } from 'gstate';
import { SearchBar } from 'components/Index/SearchBar';
import { User } from 'components/User';
import { Selected } from 'components/Selected';
import { FriendList } from 'components/Index/FriendList';
import { reducePromises } from 'tools/util';

const Splash = {
    view() {
        return m('.h2.center', [
            m('.flex.flex-column', [
                m('span.my1', 'compare game libraries with friends'),
                m('span.my1', 'filter by platform or category'),
                m('span.my1', 'figure out what game to play tonight'),
                m('span.h4.my1', '(p.s. make sure your account is set to ',
                    m('a.teal', { href: 'https://support.steampowered.com/kb_article.php?ref=4113-YUDH-6401' }, 'public'),
                    ')'
                )
            ])
        ]);
    }
}

export const Index = {
    oninit() {
        if (gstate.games()) gstate.games(null);
    },

    view({state}) {
        return [
            m(SearchBar),

            !gstate.friends
                ? m(Splash)
                : null
            ,

            gstate.user && gstate.friends
                ? [m(User), m(Selected), m(FriendList)]
                : null
            ,
        ];
    }
};