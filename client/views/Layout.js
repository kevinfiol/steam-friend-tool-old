import m from 'mithril';
import { gstate } from 'gstate';

export const Layout = {
    view({children}) {
        return m('.px3', [
            m('.my2', m('a.h1.white', { href: '/' }, 'steam friend night')),

            // Child View
            !gstate.error() && !gstate.loading()
                ? children
                : null
            ,

            gstate.loading()
                ? m('.loading.loading-lg.p2')
                : null
            ,

            gstate.progressMsg()
                ? m('h4.center', gstate.progressMsg())
                : null
            ,

            gstate.progress()
                ? m('progress.progress', { value: gstate.progress().percent }, gstate.progress().percent)
                : null
            ,

            gstate.error() && !gstate.loading()
                ? m('.h3.p4.my3.center.border.rounded', getErrorMessage(gstate.error()))
                : null
            ,
        ]);
    }
}

function getErrorMessage(error) {
    const errors = {
        1: m('.flex.flex-column', [
            m('span', 'something went wrong. :('),
            m('span', 'does your profile exist?'),
            m('span', 'or do you have ', 
                m('a.teal', { href: 'https://support.steampowered.com/kb_article.php?ref=4113-YUDH-6401' }, 'privacy settings on?')
            )     
        ]),
        2: m('.flex.flex-column', [
            m('span', 'an error occured'),
            m('span', 'you may have entered an invalid ID'),
            m('span', m('a.teal', { href: '/' }, 'click here to go back.'))
        ])
    };

    return errors[error] || 'an error occured';
}