import m from 'mithril';
import stream from 'mithril/stream';

export const gstate = {
    // User Data
    steamid: null,
    user: null,

    // Result Data
    friends: null,
    games: null,
    filteredGames: null,
    categoryDict: null,

    // Filters
    categoryFilter: null,
    platformFilter: null,
    enablePlatformFilter: null,

    // State
    selected: [],
    error: null,
    loading: null,
    progress: null,
    progressMsg: null,

    init() {
        this.games = stream(null);
        this.categoryFilter = stream(null);
        this.platformFilter = stream('linux'); // Filter linux by default
        this.enablePlatformFilter = stream(false);

        this.error = stream(null);
        this.loading = stream(false);
        this.progress = stream(null);
        this.progressMsg = stream(null);

        this.filteredGames = stream.combine((games, categories, platform, enablePlatform) => {
            if (games() && categories()) {
                return games().filter((game) => {
                    const hasCategory = categories().some(cat => game.categories.indexOf(cat) > -1);
                    const hasPlatform = enablePlatform() ? game.platforms[platform()] : true;

                    return hasCategory && hasPlatform;
                });
            }

            return [];
        }, [this.games, this.categoryFilter, this.platformFilter, this.enablePlatformFilter]);
    },

    clearData() {
        this.steamid = null;
        this.user = null;
        this.friends = null;
        this.games(null);
        
        this.selected = [];
        this.error(null);
    }
};