steam friend night
---

```
pipenv shell
set FLASK_APP=application.py
flask run
```

[Demo available here](https://sfn.herokuapp.com)

A single-page application built with Mithril, Flask, and PostgreSQL to allow users to view and compare what games they have in common with friends. The user can compare their libraries with multiple friends to narrow game suggestions.

The application uses Valve's public Steam Storefront API + Community API to fetch a user's friend's list, and then matches games in their respective libraries. The interface also allows for filtering of the search results by genres and operating systems, without page reloads. The application is able to retrieve results relatively quickly because each game is stored/retrieved on Heroku Postgres.

This was made for those Friday nights when you're on VOIP with friends and no one can decide what multi-player game to play. Including the friend who only games on Linux.