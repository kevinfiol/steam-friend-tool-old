var path = require('path');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: './client/index.js',
    resolve: {
        alias: {
            views: path.resolve(__dirname, 'client/views/'),
            components: path.resolve(__dirname, 'client/components/'),
            tools: path.resolve(__dirname, 'client/tools/'),
            gstate: path.resolve(__dirname, 'client/gstate')
        }
    },
    plugins: [
        new UglifyJsPlugin(),
        new MiniCssExtractPlugin({
            filename: "./css/[name].css",
            chunkFilename: "[id].css"
        }),
    ],
    output: {
        path: path.resolve(__dirname, './server/static'),
        filename: 'app.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                // handles css rules like background: url(...)
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'url-loader'
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ],
    }
};